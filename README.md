<div align="center">
    <h1> Denovo (Un-official Ovo API wrapper for Deno 🦕)</h1>
    
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://gitlab.com/satyawikananda/denovo/-/raw/master/LICENSE)
</div>

<div align="center">
    <img src="./screenshot/thumbnail.png" align="center" width="600px">
</div>

# Description

This is an Un-official Ovo API wrapper for Deno got inspired from [ovoid-nodejs](https://github.com/apriady/ovoid-nodejs) and this is the result of porting from [ovoid](https://github.com/lintangtimur/ovoid/)

# Documentation

Wait a minute ...

# Features

| Method  | Result  |
|---|---|
| `login2FA`  | ✔ |
| `login2FAVerify`  | ✔ |
| `loginSecurityCode`  | ✔  |
| `getBudget`  | ✔  |
| `getRefBank`  | ✔  |
| `getAllNotification`  | ✔  |
| `getUnreadHistory`  | ✔  |
| `logout`  | ✔  |
| `generateTrxId`  | ✖  |
| `transferOvo`  | ✖  |

# Author
[@Satyawikananda](https://instagram.com/satyawikananda) © 2020